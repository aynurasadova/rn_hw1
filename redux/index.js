import { createStore, combineReducers } from "redux";
import { planReducer } from "./data";
import { userReducer } from "./user";

const rootReducer = combineReducers({
    data: planReducer,
    user: userReducer,
}) 

const store = createStore(rootReducer);
export default store;