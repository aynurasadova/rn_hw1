const CHANGE_USER_INFO = "CHANGE_USER_INFO";
const GET_USER_FROM_AS = "GET_USER_FROM_AS";
const ERROR_IMG = "ERROR_IMG";

const MODULE_NAME = "user";
export const getUser = (state) => state[MODULE_NAME].user;

const initialState = {
    user: 
    {
        photo: "https://res.cloudinary.com/de8cn98oh/image/upload/v1590507932/defaultProfile_hsvpbg.png",
        username: "username",
    }
}

export function userReducer(state = initialState, {type, payload}) {
    switch (type) {
        case GET_USER_FROM_AS:
            return{
                ...state,
                user: payload
            }
        case CHANGE_USER_INFO:
            return {
                ...state,
                user:
                    {
                        photo: payload.imgUri,
                        username: payload.userName,
                    }
            }
        case ERROR_IMG:
            return {
                ...state,
                user: {
                    ...state.user,
                    photo: payload.defaultPhoto
                }
            }
        default:
            return state;
    }

}

//Action Creators


export const getUserFromAS = (payload) => ({
    type: GET_USER_FROM_AS,
    payload,
})

export const changeUserInfo = (payload) => ({
    type: CHANGE_USER_INFO,
    payload,
})

export const errorImg = (payload) => ({
    type: ERROR_IMG,
    payload
})
