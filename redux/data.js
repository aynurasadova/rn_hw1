import { createID } from "../utils/uniqueID";

//Action Types
const GET_LISTS_FROM_AS = "GET_LISTS_FROM_AS";
const ADD_LIST = "ADD_LIST";
const ADD_TODO = "ADD_TODO";
const DELETE_TODO = "DELETE_TODO";
const UPDATE_TODO = "UPDATE_TODO";
const TOGGLE_TODO_STATE = "TOGGLE_TODO_STATE";
const RESET_TODO = "RESET_TODO";
const DELETE_LIST = "DELETE_LIST";
const NOT_SAVED_TODOS = "NOT_SAVED_TODOS";

//Get Actions
const MODULE_NAME = "data";
export const getLists = (state) => state[MODULE_NAME].lists;

//Reducer
const initialState = {
    lists: [],
}

export function planReducer(state = initialState, {type, payload}) {
    switch (type) {
        case GET_LISTS_FROM_AS: 
            return {
                ...state,
                lists: payload,
            }
        case ADD_LIST:
            return {
                ...state,
                lists:[
                    ...state.lists,
                    {
                        id: payload.id,
                        name: payload.name,
                        total: 0,
                        todos: [],
                        regular: payload.regular,
                    }
                ]

            }
        case ADD_TODO: 
            return {
                ...state,
                lists: state.lists.map((list) =>{
                    if(list.id === payload.listID) {
                        return {
                            ...list,
                            todos: [
                                {
                                    id: createID(),
                                    name: payload.todoName,
                                    unit: payload.todoUnit,
                                    amount: payload.todoAmount,
                                    done: false,
                                },
                                ...list.todos,
                            ]
                        }
                    }
                    return list;
                })
            }
        case DELETE_TODO: 
            return {
                ...state,
                lists: state.lists.map((list) => {
                    if(list.id === payload.listID)
                    {
                        return {
                            ...list,
                            todos: list.todos.filter((todo) => todo.id !== payload.todoID)
                        }
                    }
                    return list;
                })
            }
        case UPDATE_TODO: 
            return {
                ...state,
                lists: state.lists.map((list) => {
                    if(list.id === payload.listID) {
                        return {
                            ...list,
                            todos: list.todos.map((todo) => {
                                if(todo.id === payload.todoID) {
                                    return {
                                        ...todo,
                                        name: payload.todoName,
                                        unit: payload.todoUnit,
                                        amount: payload.todoAmount,
                                    }
                                }
                                return todo;
                            })
                        }
                    }
                    return list;
                })
            }
        case TOGGLE_TODO_STATE: 
            return {
                ...state, 
                lists: state.lists.map((list) => {
                    if(list.id === payload.listID) {
                        return {
                            ...list,
                            todos: list.todos.map((todo) => {
                                if(todo.id === payload.todoID) {
                                    return {
                                        ...todo,
                                        done: !todo.done,
                                    }
                                }
                                return todo;
                            })
                        }
                    }
                    return list;
                })
            }
        case RESET_TODO:
            return {
                ...state, 
                lists: state.lists.map((list) => {
                    if(list.id === payload.listID) {
                        return {
                            ...list,
                            todos: list.todos.map((todo) => {
                                {
                                    return {
                                        ...todo,
                                        done: false,
                                    }
                                }
                            })
                        }
                    }
                    return list;
                })
            }   
        case NOT_SAVED_TODOS: 
            return {
                ...state,
                lists: state.lists.map((list) => {
                    if(list.id === payload.listID) {
                        return {
                            ...list,
                            todos: payload.notSavedTodos
                        }
                    }
                    return list;
                })
            }
        case DELETE_LIST: 
            return {
                ...state,
                lists: state.lists.filter((list) => list.id !== payload.listID)
            }
        default:
            return state;
    }

}

//Action Creators

export const getListFromAS = (payload) => ({
    type: GET_LISTS_FROM_AS,
    payload,
})

export const addList = (payload) => {
    return {
        type: ADD_LIST,
        payload,
    }
}

export const addTodo = (payload) => ({
    type: ADD_TODO,
    payload,
})

export const updateTodo = (payload) => ({
    type: UPDATE_TODO,
    payload
})

export const deleteTodo = (payload) => ({
    type: DELETE_TODO,
    payload,
})

export const toogleTodoState = (payload) => ({
    type: TOGGLE_TODO_STATE,
    payload,
})

export const resetTodo = (payload) => ({
    type: RESET_TODO,
    payload
})

export const deleteList = (payload) => ({
    type: DELETE_LIST,
    payload
})

export const notSaveTodosFunc = (payload) => ({
    type: NOT_SAVED_TODOS,
    payload,
})