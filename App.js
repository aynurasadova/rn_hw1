import React, { useState, useEffect } from 'react';
import { Provider, connect,} from 'react-redux';
import { StatusBar, AsyncStorage } from 'react-native';
import { AppLoading } from 'expo';

import { 
  getListFromAS, 
  getLists, 
} from './redux/data';

import { 
  getUser, 
  getUserFromAS
} from './redux/user';

import store from './redux';
import { RootNav } from './navigation';
import { loadFonts } from './style/fonts';
import { DefText } from './commons';

const mapStateToProps = (state) => ({
  lists: getLists(state),
  users: getUser(state),
})

const Index = connect(mapStateToProps, { getListFromAS, getUserFromAS })(({
  getListFromAS, 
  lists, 
  users, 
  getUserFromAS
}) => {

        useEffect(() => {
          const getListsFunction = async() => {
            const lists = await AsyncStorage.getItem("Lists");
            // console.log(lists)
            if(lists) {
              getListFromAS(JSON.parse(lists))
            }
          }
          getListsFunction();
        }, [])

        useEffect(() => {
          const getUserFunction = async () => {
            const user = await AsyncStorage.getItem("user");
            console.log(user)
            if(user) {
              getUserFromAS(JSON.parse(user))
            }
          }
          getUserFunction();
        }, [])

        useEffect(() =>{
          const setListToAS = async () => await AsyncStorage.setItem("Lists",JSON.stringify(lists))
          if(lists) {
            setListToAS();
          }
        },[lists])

        useEffect(() => {
          const setUserToAS = async () => await AsyncStorage.setItem("user", JSON.stringify(users))
          if(users) {
            setUserToAS();
          }
        },[users])


  return (
        <>
          <StatusBar hidden = {true} />
          <RootNav />
        </>
  );  
})

const  App = () => {
  const [loaded, setLoaded] = useState(false);
  
  if(!loaded) {
    return(
      <AppLoading 
        startAsync = {loadFonts}
        onFinish = {() =>setLoaded(true)}
        onError = {() => console.log("Something went wrong!!")}
      />
      )
    }

    return (
      <Provider store = {store}>
        <Index />
      </Provider>
);
    
}

export default App;