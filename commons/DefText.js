import React from 'react';
import { Text } from 'react-native';

export const fontFamilies = {
    bold: "MontserratBold",
    medium: "MontserratMedium",
    regular: "MontserratRegular",
}

export const DefText = ({weight, children, style, ...rest}) => {
    return(
    <Text {...rest} style = {[{fontFamily: fontFamilies[weight] || fontFamilies.medium}, style]}>{children}</Text>
    )
}