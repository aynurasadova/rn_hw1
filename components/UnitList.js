import React, {useState} from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native'

import COLORS from '../style/color';
import { DefText, } from '../commons';

export const UnitList = ({unit, styleUnitTextOnFocus, ...rest}) => {  
    
    return (
            <TouchableOpacity {...rest} style = {styles.todoUnit}>
              <DefText weight = {styleUnitTextOnFocus ? "bold" : "regular"} style = {[styles.unitText, styleUnitTextOnFocus ? styles.onFocus : ""]}>{unit}</DefText>
            </TouchableOpacity>
            
    );
  };
  
  const styles = StyleSheet.create({
    todoUnit: {
      width: "22%",
      backgroundColor: COLORS.lightGrey,
      borderRadius: 25,
      justifyContent: "center",
      paddingHorizontal: 20,
    },
    unitText: {
      textAlign: "center",
      color: "grey",
      fontSize: 13,
    },
    onFocus: {
        color: "black",
    }
  });
  