import React, { useState, useEffect } from 'react';
import { 
  StyleSheet, 
  View, 
  TouchableOpacity, 
  FlatList, 
  TextInput, 
  Keyboard, 
  Alert, 
  Dimensions 
} from 'react-native'
import { connect } from 'react-redux';

import { 
  getLists, 
  addTodo, 
  deleteTodo, 
  updateTodo, 
  toogleTodoState, 
  resetTodo,
  notSaveTodosFunc,
} from '../../redux/data';
import {DefText, Layout, fontFamilies } from '../../commons';
import COLORS from '../../style/color';
import images from '../../style/images';
import { TodoList, UnitList } from '../../components';

const unitFamilies = {
  pkg: "pkg",
  kg: "kg",
  litre: "litre",
  bott: "bott",
}

const mapStateToProps = (state) => ({
  lists: getLists(state)
})

export const SingleListDetail = connect(mapStateToProps, {
  addTodo,
  deleteTodo,
  updateTodo,
  toogleTodoState,
  resetTodo,
  notSaveTodosFunc
})(({addTodo, deleteTodo, updateTodo, toogleTodoState, resetTodo, navigation, lists, route, notSaveTodosFunc}) => {

    const {
      params: { listID, regularForReset, typeIsRegular }
    } = route;
    const {name = "404", todos = [], bought = 0, total = 0} = lists.find((list) => list.id === listID) || {};

    //Todos, 
    const totalTodos = todos.length;
    const empty = totalTodos === 0;
    let boughtTodos = 0;
    for(let todo in todos) {
      if(todos[todo].done) {
        boughtTodos++;
      }
    }
    const [todoItem, setTodoItem] = useState({
      todoName: "",
      todoUnit: "",
      todoAmount: "",
    })

    //not Saved list handler
    const [notSavedTodos, setNotSavedTodos] = useState(!!todos ? todos : {});

    //States of logics
    const [isAddingTodo, setIsAddingTodo] = useState(empty ? true : false); //FALSE
    const [editMode, setEditMode] = useState(false);
    const [editID, setEditID] = useState(null);

    const clearFields = () => {
      setTodoItem({
        todoName: "",
        todoUnit: "",
        todoAmount: "",
      });
      setUnit("");
      setEditMode(false);
      setEditID(null);
      Keyboard.dismiss();
    }

    //Taking values from fields
      const todoFieldChangeHandler = (name, value) => {
        setTodoItem((todos) => ({
          ...todos,
          [name]: value,
        })
        );
      } 
      
    const [unit, setUnit] = useState("");
    
    const setUnitFunction = (name) =>{
      setUnit(name);
    }

    //Changing Amount
    const wrongAmountChecker = +(todoItem.todoAmount) <= 0;
    const amountIncreasingFunction = () => {
      setTodoItem((todos) => ({
        ...todos,
        todoAmount: (+todoItem.todoAmount + 1).toString()
      }))
    } 
    const amountDecreasingFunction = () => {
      setTodoItem((todos) => ({
        ...todos,
        todoAmount: wrongAmountChecker ? "0" : (+todoItem.todoAmount  - 1).toString()
      }))
    } 

    //Submit form
    const submitTodoForm = () => {
      for(let todo in todoItem) {
        if(todoItem[todo].trim() === "") {
          Alert.alert("Form-Field Validation", "fill the fields correctly");
          return;
        }
      }
      if(isNaN(todoItem.todoAmount))
        {
          Alert.alert(`enter a number for count`);
          return;
        }
        else if( +(todoItem.todoAmount) <= 0) {
          Alert.alert(`Amount is incorrect`, `Change amount`);
          return;
        }
      const args = {
        listID: listID,
        todoName: todoItem.todoName, 
        todoUnit: todoItem.todoUnit, 
        todoAmount: todoItem.todoAmount
      };
      if(!editMode) {
        addTodo(args);
        setTodoItem({
          todoName: "",
          todoUnit: "",
          todoAmount: "",
        });
        setUnit("");
        Keyboard.dismiss();
      }
      else if(editMode) {
        updateTodo({...args,todoID: editID});
        setTodoItem({
          todoName: "",
          todoUnit: "",
          todoAmount: "",
        });
        setUnit("");
        setEditMode(false);
        setEditID(null);
        Keyboard.dismiss();
      }
    }

    // Delete Todo
    const handleDelete = (todoID) => {
      Alert.alert("Do you really want to delete?","If yes click delete", [
        {text: "Cancel", style: "cancel"},
        {
          text: "Delete",
          onPress: () => {
            deleteTodo({listID, todoID})
            setEditMode(false);
            setEditID(null);
        }
        }
      ])
    }

    //Canceling editing Product
    const handleCancel = () => {
        clearFields();
    }

    //Edit products
    const handleEdit = (item) => {
      setTodoItem((fields) => ({
        ...fields,
        todoName: item.name,
        todoUnit: item.unit.toString(),
        todoAmount: item.amount.toString(),
      }))
      setUnit(item.unit)
      setEditMode(true)
      setEditID(item.id)
    }

    //Toggle Todo
    const handleToggle = (todoID) => {
      toogleTodoState({listID, todoID})
    }

    //Reset
    const resetTodos = () => {
      Alert.alert("Are you sure to reset all item ?", "If yes, click reset", [
        {
          text: "Cancel",
          style: "cancel",
        },
        {
          text: "Reset",
          onPress: () => resetTodo({listID})
        }
    ])
      // resetTodo({listID})
    }

    const goBackHandler = () => {
      if(isAddingTodo) {
        notSaveTodosFunc({ listID, notSavedTodos : notSavedTodos });
        if(notSavedTodos.length === 0) {
          typeIsRegular || regularForReset ? navigation.navigate("Regular") : navigation.navigate("OneTime")
        } else {
          setIsAddingTodo((v) => !v)
        }
      }
      else {
        typeIsRegular || regularForReset ? navigation.navigate("Regular") : navigation.navigate("OneTime")
      }
      clearFields();
    };
    const addingNewTodoHandler = () => {
      if(!isAddingTodo) {
        setNotSavedTodos(todos);
      }
      setIsAddingTodo(!isAddingTodo)
      clearFields();
    };
    
    //sorting todos
    const sortedTodos = todos.sort((a,b) =>{ 
      if(a.done === b.done) return 0;
      if(a.done) return 1;
      if(b.done) return -1;
    })

    return (
      <Layout 
        title = {name}
        goBackDisabled = {false} 
        goBack = {goBackHandler}
        rightCornerBtn = {true}
        onPress = {addingNewTodoHandler}
        imgSource = {!isAddingTodo ? images.newNoteIcon : images.saveImg}
        btnStyle = {styles.newNoteIcon}
      >
        { isAddingTodo &&
          <View style = {styles.addNewTodo}>
            <View style = {{flexDirection: "row", height: 15}}>
              <DefText weight = "medium" style = {styles.positionNameText}>position name</DefText>
              <DefText weight = "medium" style = {styles.countText}>count</DefText>
            </View>
            <View style = {styles.row}>
              <View style = {styles.todoName}>
                <TextInput 
                  value = {todoItem.todoName}
                  onChangeText = {(value) => todoFieldChangeHandler("todoName", value)}
                  style = { {textAlign: "center", fontWeight: "bold"} } 
                />
              </View>

              <View style = {styles.todoAmount}>
                <View style = {styles.rowOfSigns}>
                  <TouchableOpacity onPress = {amountDecreasingFunction}>
                    <DefText style = {styles.signText} weight = "bold">-</DefText>
                  </TouchableOpacity>
                  <View style = {styles.inputAmount}>
                    <TextInput 
                      value = {todoItem.todoAmount}
                      keyboardType = "numeric"
                      onChangeText = {(value) => todoFieldChangeHandler("todoAmount", value)}
                      style = {{width: 20, textAlign: "center",fontFamily: fontFamilies.bold}}
                    />
                  </View>
                  <TouchableOpacity onPress = {amountIncreasingFunction}>
                    <DefText style = {styles.signText} weight = "bold">+</DefText>
                  </TouchableOpacity>
                </View>                
              </View>
            </View>

            <View style = {styles.row}>
              <UnitList 
                key = {unitFamilies.pkg} 
                unit = {unitFamilies.pkg}
                onPress = {() => {
                  setUnitFunction(unitFamilies.pkg)
                  todoFieldChangeHandler("todoUnit", unitFamilies.pkg)
                }}
                styleUnitTextOnFocus = {unit === unitFamilies.pkg ? true : false}
              />
              <UnitList 
                key = {unitFamilies.kg} 
                unit = {unitFamilies.kg}
                onPress = {() => {
                  setUnitFunction(unitFamilies.kg)
                  todoFieldChangeHandler("todoUnit", unitFamilies.kg)
                }}
                styleUnitTextOnFocus = {unit === unitFamilies.kg ? true : false}
              />
              <UnitList 
                key = {unitFamilies.litre} 
                unit = {unitFamilies.litre}
                onPress = {() => {
                  setUnitFunction(unitFamilies.litre)
                  todoFieldChangeHandler("todoUnit", unitFamilies.litre) 
                }}
                styleUnitTextOnFocus = {unit === unitFamilies.litre ? true : false}
              />
              <UnitList 
                key = {unitFamilies.bott} 
                unit = {unitFamilies.bott}
                onPress = {() => {
                  setUnitFunction(unitFamilies.bott)
                  todoFieldChangeHandler("todoUnit", unitFamilies.bott)
                }}
                styleUnitTextOnFocus = {unit === unitFamilies.bott ? true : false}
              />
            </View>

            {
              editMode &&
              <View style = {styles.rowEditBtns}>
                  <TouchableOpacity 
                    onPress = {handleCancel}
                    style = {[styles.actionBtns, styles.cancelBtn, styles.Btn]}>
                      <DefText weight = "bold" style = {[styles.BtnText]}>
                        Cancel
                      </DefText>
                  </TouchableOpacity>

                  <TouchableOpacity
                    onPress = {submitTodoForm} 
                    style = {[styles.actionBtns, styles.Btn]}>
                      <DefText weight = "bold" style = {[styles.BtnText]}>
                        Update
                      </DefText>
                  </TouchableOpacity>
              </View>
            }

            { !editMode &&
            <TouchableOpacity onPress = {submitTodoForm} style = {styles.Btn} >
              <DefText weight = "bold" style = {styles.BtnText}>Add to List</DefText>
            </TouchableOpacity>
            }
          </View>
        }

        {
          totalTodos !==0 &&
          <>
          { !isAddingTodo &&
              <View style = {[styles.headingRow, {marginVertical: regularForReset ? 0 : 10}]}>

                { regularForReset &&
                  <View style = {styles.resetBtnWrapper}>
                    <TouchableOpacity onPress = {resetTodos} style = {styles.resetBtn}>
                      <DefText weight = "bold" style = {styles.resetBtnText}>Reset</DefText>
                    </TouchableOpacity>
                  </View>
                }

                <View style = {styles.todosCounterWrapper}>
                  <DefText style = {styles.todosCounterText}>{boughtTodos} / {totalTodos}</DefText>
                </View>

              </View>
          }

            <FlatList 
              showsVerticalScrollIndicator = {false}
              contentContainerStyle = {styles.contentContainerStyle}
              data = {sortedTodos}
              renderItem = {({item}) => (
                <TodoList 
                  isStatic = {isAddingTodo ? false: true}
                  isAddingMode = {isAddingTodo ? true : false}
                  key = {item.id} 
                  todo = {item}
                  isOnEditMode = {editMode}
                  toggleOnPress = {() => handleToggle(item.id)}
                  editOnPress = {() => handleEdit(item)}
                  deleteOnPress = {() =>handleDelete(item.id)}
                />
                )}
              />
                
        </>
        }
      </Layout>   
    );
  });
  
  const styles = StyleSheet.create({
    boldUnit: {
      color: "red",
      // fontWeight: "",
    },
    newNoteIcon: {
      width: 19,
      height: 19,
    },
    headingRow: {
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "center",
      width: "100%",
      paddingHorizontal: 4,
      // marginTop: 10,
      // marginHorizontal: 15,
      // borderWidth: 2,
    },
    resetBtn: {
      width: 72,
      height: 20,
      backgroundColor: COLORS.red,
      borderRadius: 20
    },
    resetBtnText: {
      fontSize: 10,
      textTransform: "uppercase",
      textAlign: "center",
      paddingVertical: 3,
      color: "white",
    },
    contentContainerStyle: {
      marginVertical: 7
    },
    addNewTodo: {
      // borderColor: "black",
      // borderWidth: 2,
      // paddingBottom: 10,
    },
    row: {
      flexDirection: "row",
      justifyContent: "space-between",
      height: 42,
      marginVertical: 8,
    },
    todoName: {
      backgroundColor: COLORS.lightGrey,
      textAlign: "center",
      width: 260,
      height: "100%",
      borderRadius: 25,
      paddingHorizontal: 25,
      paddingVertical: 10
    },

    todoAmount: {
      backgroundColor: COLORS.lightGrey,
      width: 88,
      textAlign: "center",
      height: "100%",
      borderRadius: 25,
      paddingHorizontal: 15,
      paddingVertical: 10
    },
    rowOfSigns: {
      flexDirection: "row", 
      justifyContent: "space-between",
      marginVertical: -2, 
      marginHorizontal: -10,
      overflow: "hidden",
    },
    inputAmount: {
      textAlign: "center",
      paddingLeft: 8,
      paddingRight: 6,
    },
    signText: {
      fontSize: 18,
      paddingHorizontal: 5,
    },
    Btn: {
      backgroundColor: COLORS.red,
      height: 42,
      borderRadius: 25,
      justifyContent: "center",
      alignItems: "center",
      marginVertical: 8,
    },
    BtnText: {
      color: "white",
      textTransform: "uppercase",
      fontSize: 14,
      // textAlign: "center"
    },
    rowEditBtns: {
      flexDirection: "row",
      justifyContent: "space-between",
    },
    actionBtns: {
      width: "45%",
    },
    cancelBtn: {
      opacity: 0.5
    },
    onDoneStyle: {
      opacity: 0.5,
    },
    todosCounterWrapper: {
      position: "absolute",
      right: 11
    },
    positionNameText: {
      position: "absolute", 
      fontSize: 12, 
      left: 0, 
      paddingLeft: (Dimensions.get("screen").width)/4.7,
      color: "rgba(0,0,0,0.7)",
    },
    countText: {
      position: "absolute", 
      fontSize: 12, 
      right: 0, 
      paddingRight: (Dimensions.get("screen").width)/15.5,
      color: "rgba(0,0,0,0.7)"
    }
  });
  

 // const [boldStyled, setBoldStyled] = useState({});
//  const setUnitFunction = (name) =>{
  // setBoldStyled(() => ({
  //     [name]: true,
  // }))
  // setUnit(name);
// }

// const unitChangeHandler = (units) => {
//   let unitListReturn = [];
//   for(let unit in units){
//     unitListReturn.push(<UnitList 
//     onPress = {()=> {
//       setUnitFunction(units[unit])
//     }}  
//     key = {units[unit]} 
//     unit = {units[unit]}
//     />
//     )
//   }
//   return unitListReturn;
// }

// console.log(unitm);
