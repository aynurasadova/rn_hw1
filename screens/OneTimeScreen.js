import React, { Component } from 'react';
import { connect } from 'react-redux';
import { FlatList, } from 'react-native';

import images from '../style/images';
import { getLists } from '../redux/data';
import { ListSection } from '../components';
import { Layout } from '../commons';


const mapStateToProps = (state) =>({
        lists: getLists(state)
    }    
)
//CLASS COMPONENT
  class OneTimeScreen extends Component {    
    render() {
      const { lists, navigation } = this.props;
      return (
        <Layout 
        title = "One Time List" 
        rightCornerBtn = {true} 
        imgSource = {images.drawerImg} 
        onPress = {() => navigation.openDrawer()}
        goBackDisabled = {true}
      >
        <FlatList 
          showsVerticalScrollIndicator = {false}
          data = {lists}
          renderItem = {({item}) => (
            <ListSection 
              key = {item.id}   
              regularity = {false} 
              list = {item} 
              />
            )
          }        
        />
      </Layout>
      )
    }
  }

  export default connect(mapStateToProps)(OneTimeScreen) 

  //Functional Component
// export const OneTimeScreen = connect(mapStateToProps)((props) => {
//   const {navigation, lists} = props;
//   const drawerHandler = () => navigation.openDrawer();


//     return (
//       <Layout 
//         title = "One Time List" 
//         rightCornerBtn = {true} 
//         imgSource = {images.drawerImg} 
//         onPress = {drawerHandler}
//         goBackDisabled = {true}
//       >
//         <FlatList 
//           showsVerticalScrollIndicator = {false}
//           data = {lists}
//           renderItem = {({item}) => (
//             <ListSection 
//               key = {item.id}   
//               regularity = {false} 
//               list = {item} 
//               />
//             )
//           }        
//         />
//       </Layout>
//     );
//   });